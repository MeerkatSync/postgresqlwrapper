import XCTest

import PostgreSQLWrapperTests

var tests = [XCTestCaseEntry]()
tests += PostgreSQLWrapperTests.allTests()
XCTMain(tests)
