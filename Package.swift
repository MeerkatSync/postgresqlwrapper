// swift-tools-version:5.1
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "PostgreSQLWrapper",
    products: [
        .library(
            name: "PostgreSQLWrapper",
            targets: ["PostgreSQLWrapper"]),
    ],
    dependencies: [
        .package(url: "https://gitlab.com/MeerkatSync/meerkatcore", .branch("develop")),
        .package(url: "https://github.com/IBM-Swift/Swift-Kuery-PostgreSQL.git", from: "2.1.1")

    ],
    targets: [
        .target(
            name: "PostgreSQLWrapper",
            dependencies: ["MeerkatCore"]),
        .testTarget(
            name: "PostgreSQLWrapperTests",
            dependencies: ["PostgreSQLWrapper"]),
    ]
)
